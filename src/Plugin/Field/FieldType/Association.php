/**
 * @file
 * Contains \Drupal\genealogy\Plugin\Field\FieldType\Association.
 */

namespace Drupal\genealogy\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'association' field type.
 *
 * @FieldType (
 *   id = "association",
 *   label = @Translation("Association"),
 *   description = @Translation("Describes a relationship between individuals."),
 *   default_widget = "association",
 *   default_formatter = "association"
 * )
 */
class Association extends FieldItemBase {
  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'individual' => [
          'type' => 'reference',
          'target_type' => 'individual',
        ],
        'relationship' => [
          'type' => 'string',
          'size' => 25
        ],
        'note' => [
          'type' => 'reference',
          'target_type' => 'note',
          'number' => 'unlimited',
        ],
        'reference' => [
          'type' => 'reference',
          'target_type' => 'reference',
          'number' => 'unlimited',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
  */
  public function isEmpty() {
    $value1 = $this->get('number')->getValue();
    $value2 = $this->get('sides')->getValue();
    $value3 = $this->get('modifier')->getValue();
    return empty($value1) && empty($value2) && empty($value3);
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Add our properties.
    $properties['number'] = DataDefinition::create('integer')
      ->setLabel(t('Number'))
      ->setDescription(t('The number of dice'));

    $properties['sides'] = DataDefinition::create('integer')
      ->setLabel(t('Sides'))
      ->setDescription(t('The number of sides on each die'));

    $properties['modifier'] = DataDefinition::create('integer')
      ->setLabel(t('Modifier'))
      ->setDescription(t('The modifier to be applied after the roll'));

    $properties['average'] = DataDefinition::create('float')
      ->setLabel(t('Average'))
      ->setDescription(t('The average roll produced by this dice setup'))
      ->setComputed(TRUE)
      ->setClass('\Drupal\dicefield\AverageRoll');

    return $properties;
  }
}
